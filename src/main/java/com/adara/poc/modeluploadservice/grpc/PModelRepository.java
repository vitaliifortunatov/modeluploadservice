package com.adara.poc.modeluploadservice.grpc;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PModelRepository extends CrudRepository<PModelEntryDomain, Integer> {
}
