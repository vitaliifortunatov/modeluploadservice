package com.adara.poc.modeluploadservice.grpc;

import com.adara.poc.modeluploadservice.ModelUploadServiceGrpc;
import com.adara.poc.modeluploadservice.UploadPModelRequest;
import com.adara.poc.modeluploadservice.UploadPModelResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ModelUploadServiceClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(ModelUploadServiceClient.class);

  private ModelUploadServiceGrpc.ModelUploadServiceBlockingStub modelUploadService;

  @PostConstruct
  private void init() {
    ManagedChannel managedChannel = ManagedChannelBuilder
            .forAddress("localhost", 6565).usePlaintext().build();

    modelUploadService = ModelUploadServiceGrpc.newBlockingStub(managedChannel);
  }

  public Integer uploadModel(PModelEntryDomain entry) {
    UploadPModelRequest request = entry.toUploadPModelRequestProto();
    LOGGER.info("client sending {}", request);

    UploadPModelResponse response = modelUploadService.uploadPModel(request);
    LOGGER.info("client received {}", response);

    return response.getId();
  }
}
