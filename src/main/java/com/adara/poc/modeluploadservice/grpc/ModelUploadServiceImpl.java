package com.adara.poc.modeluploadservice.grpc;

import com.adara.poc.modeluploadservice.UploadPModelResponse;
import com.adara.poc.modeluploadservice.ModelUploadServiceGrpc;
import com.adara.poc.modeluploadservice.UploadPModelRequest;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@GRpcService
public class ModelUploadServiceImpl extends ModelUploadServiceGrpc.ModelUploadServiceImplBase {

    private final PModelRepository repository;

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ModelUploadServiceImpl.class);

    public ModelUploadServiceImpl(PModelRepository repository) {
        this.repository = repository;
    }

    @Override
    public void uploadPModel(UploadPModelRequest request, StreamObserver<UploadPModelResponse> responseObserver) {
        LOGGER.info("server received {}", request);

        PModelEntryDomain entry = PModelEntryDomain.fromProto(request);
        entry = repository.save(entry);
        UploadPModelResponse response = UploadPModelResponse.newBuilder().setId(entry.getpModelId()).build();
        LOGGER.info("server responded {}", response);

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
