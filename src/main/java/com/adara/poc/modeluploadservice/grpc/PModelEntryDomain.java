package com.adara.poc.modeluploadservice.grpc;

import com.adara.poc.modeluploadservice.PModelEntry;
import com.adara.poc.modeluploadservice.UploadPModelRequest;
import com.google.protobuf.ByteString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "p_model")
public class PModelEntryDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer pModelId;

    private Integer modelId;
    private Integer campaignId;
    private String optimizationPolicy;
    private String pmml;
    private String pmodel;
    private byte[] pmodelBin;
    private boolean active;
    private Integer productId;
    @CreationTimestamp
    private LocalDateTime creationTs;
    private Integer modelTemplateId;
    @UpdateTimestamp
    private LocalDateTime modificationTs;

    public static PModelEntryDomain fromProto(PModelEntry proto) {
        PModelEntryDomain entry = new PModelEntryDomain();
        entry.setpModelId(proto.getPModelId());
        entry.setModelId(proto.getModelId());
        entry.setCampaignId(proto.getCampaignId());
        entry.setOptimizationPolicy(proto.getOptimizationPolicy());
        entry.setPmml(proto.getPmml());
        entry.setPmodel(proto.getPmodel());
        entry.setPmodelBin(proto.getPmodelBin().toByteArray());
        entry.setActive(proto.getActive());
        entry.setProductId(proto.getProductId());
        entry.setModelTemplateId(proto.getModelTemplateId());
        return entry;
    }

    public static PModelEntryDomain fromProto(UploadPModelRequest proto) {
        PModelEntryDomain entry = new PModelEntryDomain();
        entry.setModelId(proto.getModelId());
        entry.setCampaignId(proto.getCampaignId());
        entry.setOptimizationPolicy(proto.getOptimizationPolicy());
        entry.setPmml(proto.getPmml());
        entry.setPmodel(proto.getPmodel());
        entry.setPmodelBin(proto.getPmodelBin().toByteArray());
        entry.setActive(proto.getActive());
        entry.setProductId(proto.getProductId());
        entry.setModelTemplateId(proto.getModelTemplateId());
        return entry;
    }

    public Integer getpModelId() {
        return pModelId;
    }

    public void setpModelId(Integer pModelId) {
        this.pModelId = pModelId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getOptimizationPolicy() {
        return optimizationPolicy;
    }

    public void setOptimizationPolicy(String optimizationPolicy) {
        this.optimizationPolicy = optimizationPolicy;
    }

    public String getPmml() {
        return pmml;
    }

    public void setPmml(String pmml) {
        this.pmml = pmml;
    }

    public String getPmodel() {
        return pmodel;
    }

    public void setPmodel(String pmodel) {
        this.pmodel = pmodel;
    }

    public byte[] getPmodelBin() {
        return pmodelBin;
    }

    public void setPmodelBin(byte[] pmodelBin) {
        this.pmodelBin = pmodelBin;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public LocalDateTime getCreationTs() {
        return creationTs;
    }

    public void setCreationTs(LocalDateTime creationTs) {
        this.creationTs = creationTs;
    }

    public Integer getModelTemplateId() {
        return modelTemplateId;
    }

    public void setModelTemplateId(Integer modelTemplateId) {
        this.modelTemplateId = modelTemplateId;
    }

    public LocalDateTime getModificationTs() {
        return modificationTs;
    }

    public void setModificationTs(LocalDateTime modificationTs) {
        this.modificationTs = modificationTs;
    }

    public UploadPModelRequest toUploadPModelRequestProto() {
        return UploadPModelRequest.newBuilder()
                .setModelId(getModelId())
                .setCampaignId(getCampaignId())
                .setOptimizationPolicy(getOptimizationPolicy())
                .setPmml(getPmml())
                .setPmodel(getPmodel())
                .setPmodelBin(ByteString.copyFrom(getPmodelBin()))
                .setActive(isActive())
                .setProductId(getProductId())
                .setModelTemplateId(getModelTemplateId())
                .build();
    }
}
