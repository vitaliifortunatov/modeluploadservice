package com.adara.poc.modeluploadservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGrpcApplication {
  public static void main(String[] args) {
    SpringApplication.run(SpringGrpcApplication.class, args);
  }
}
