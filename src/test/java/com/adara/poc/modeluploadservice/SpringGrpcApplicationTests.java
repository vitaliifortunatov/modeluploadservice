package com.adara.poc.modeluploadservice;

import static org.assertj.core.api.Assertions.assertThat;

import com.adara.poc.modeluploadservice.grpc.PModelEntryDomain;
import com.adara.poc.modeluploadservice.grpc.PModelRepository;
import com.google.protobuf.ByteString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.adara.poc.modeluploadservice.grpc.ModelUploadServiceClient;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringGrpcApplicationTests {

  @Autowired
  private ModelUploadServiceClient modelUploadServiceClient;

  @Test
  public void testInsertDummyPModelReturnsIdNotNull() {
    byte[] testByteStream = {1,2,3};
    PModelEntryDomain entry = PModelEntryDomain.fromProto(UploadPModelRequest.newBuilder()
        .setModelId(123)
        .setCampaignId(234)
        .setOptimizationPolicy("1")
        .setPmml("pmml")
        .setPmodel("pmodel")
        .setPmodelBin(ByteString.copyFrom(testByteStream))
        .setActive(true)
        .setProductId(345)
        .setModelTemplateId(456)
            .build());
    Integer response = modelUploadServiceClient.uploadModel(entry);
    assertThat(response).isNotNull();
  }
}
